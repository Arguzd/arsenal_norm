# -*- coding: utf-8 -*-
# Python 3.5

import math
import os
import platform
import random
from datetime import datetime

from pulp import * # 2.0
from psycopg2 import connect, DatabaseError # 2.8.4

# Строка подключения к базе
connStr = "host='h3.regionview.ru' port='25001' dbname='db' user='admin' password='admin'"
# Название полинома
polName = "Тестовый шаблон"
# Время, на которое рассчитываются ВБР всех элементов
averTime = 61320
# Максимальное количество решений
kSet = 200
# Число статистических испытаний
MSet = 10
# Номер алгоритма
iAlg = 3 # 1 - первый, 2 - второй, 3 - оба

# ---------------------------------------------------------------------------------------------------------------------
# Входные данные
# ---------------------------------------------------------------------------------------------------------------------

# Интерфейс для расчёта вероятностных показателей МКА
class apiMKA:

    def __init__(self,
                 polynomialName, # Название полинома
                 averageTime = 61320): # Время в часах, для которого рассчитывается ВБР элементов (по умолчанию 7 лет)

        # Чтение входных данных из базы по заданными критериям
        # Инициализация подключения
        conn = connect(os.getenv("connStr", connStr))

        try:
            # Запрос полинома
            sqlPolynomSelectQuery = """
                SELECT templ.polynomial
                FROM public.template templ
                WHERE templ.name LIKE '""" + polynomialName + """'
            """

            # Инициалзация курсора и загрузка данных
            cursorPolynomSelect = conn.cursor()
            cursorPolynomSelect.execute(sqlPolynomSelectQuery)

            # Сохранение полинома в переменную класса
            # Если ответ не пустой
            if cursorPolynomSelect.rowcount > 0:
                # Цикл по записям
                for record in cursorPolynomSelect:
                    # Запись полинома и выход из цикла
                    self.polynomial = str(record[0])
                    break

            # Закрытие курсора
            cursorPolynomSelect.close()

            # Запрос элементов конфигурации
            sqlElementsSelectQuery = """
            SELECT templEnt.pos,
                elem.id,
                elem.name,
                elem.description
            FROM public.template templ,
                public.template_entity templEnt,
                public.element elem
            WHERE templ.name LIKE '""" + polynomialName + """'
                AND templ.id = templEnt.template_id
                AND elem.id = templEnt.element_id
            ORDER BY templEnt.pos
            """

            # Инициалзация курсора и загрузка данных
            cursorElementsSelect = conn.cursor()
            cursorElementsSelect.execute(sqlElementsSelectQuery)

            # Сохранение элементов в переменную класса
            # Текущий тип элементов
            curElemId = -1
            # Конфигурация
            self.config = []
            # Слой конфигурации
            layerId = -1
            # Массив ссылок на типы элементов
            elemTypeIdArray = []
            # Количество элементов в конфигурации
            self.countElem = 0

            # Если ответ не пустой
            if cursorElementsSelect.rowcount > 0:
                # Цикл по записям
                for record in cursorElementsSelect:
                    # Если новый тип конфигурации
                    if (curElemId != int(record[1])):
                        self.config.append([])
                        curElemId = int(record[1])
                        layerId += 1
                        elemTypeIdArray.append(str(record[1]))
                    # Сохранение данных текущего слоя
                    self.config[layerId].append(int(record[0]))
                    self.countElem += 1

            # Закрытие курсора
            cursorElementsSelect.close()

            # Выгрузка данных о вариантах реализации
            # Варианты реализации каждого типа элементов (конфигурации)
            self.elementsArray = []
            # Энергоёмкость конфигураций
            self.eArray = []
            # Масса конфигураций
            self.mArray = []
            # Вероятность безотказной работы (ВБР) реализаций
            self.pArray = []
            # Список id вариантов реализации
            self.stockIdArray = []
            # Идентификатор слоя
            layerId = 0

            # Цикл по типам элементов
            for elemTypeId in elemTypeIdArray:

                # Расширение массивов
                self.elementsArray.append([])
                self.eArray.append([])
                self.mArray.append([])
                self.pArray.append([])
                self.stockIdArray.append([])

                # Запрос параметров элементов конфигурации
                sqlStocksSelectQuery = """
                SELECT st.vendor_name,
                    st.energy,
                    st.weight,
                    rel.value p_value,
                    rel.time p_time,
                    st.id
                FROM public.stock st,
                    public.reliability rel
                WHERE st.element_id = """ + elemTypeId + """
                    AND rel.stock_id = st.id
                ORDER BY st.vendor_name
                """

                # Инициалзация курсора и загрузка данных
                cursorStocksSelect = conn.cursor()
                cursorStocksSelect.execute(sqlStocksSelectQuery)

                # Id текущего элемента в рассчитываемой системе
                curElemId = 1

                # Временные массивы для расчёта ВБР
                tempValueArray = []
                tempTimeArray = []

                # Если ответ не пустой
                if cursorStocksSelect.rowcount > 0:
                    # Цикл по записям
                    for record in cursorStocksSelect:

                        # Сохраняем данные о текущем элементе
                        self.elementsArray[layerId].append(curElemId)
                        curElemId += 1
                        self.eArray[layerId].append(float(record[1]))
                        self.mArray[layerId].append(float(record[2]))
                        self.stockIdArray[layerId].append(int(record[5]))

                        tempValueArray.append(float(record[3]))
                        tempTimeArray.append(float(record[4]))

                # Закрытие курсора
                cursorStocksSelect.close()

                # Расчёт ВБР для заданного среднего времени
                for i in range(0, len(tempTimeArray)):
                    # Лямбда - параметр заданного алгоритма
                    l = -math.log(tempValueArray[i], math.e) / tempTimeArray[i]
                    self.pArray[layerId].append(math.exp(-l*averageTime))

                layerId += 1

        # Отлов ошибок соединения с базой
        except (Exception, DatabaseError) as error:
            print(error)

        # Закрытие соединения в конце работы
        finally:
            if conn is not None:
                conn.close()

    # Расчёт вероятностных показателей МКА
    def probabilitiesCalc(self,
            relevanceId, # Номер элемента по которому производится расчёт
            mod = "calc", # Режим расчёта (по умолчанию - расчёт значимости указанного элемента)
            pArray = []): # Заданные вручнуя вероятности

        # Функция расчёта полинома вероятностей
        def curPolynom(polynomial, PArray):

            # Формирование переменных
            for i in range(0, len(PArray)):
                exec("P" + str(i + 1) + "=" + str(PArray[i]))
                exec("Q" + str(i + 1) + "=" + str(1 - PArray[i]))

            # Расчёт полинома
            result = eval(polynomial)

            return result

        # Перехват ошибки входных данных
        if (relevanceId > self.countElem):
            raise Exception('Ошибка ввода данных - рассчитывается значимость несуществующего элемента')

        # Вероятности безотказной работы элементов по умолчанию
        PArray1 = [0.5 for x in range(self.countElem)]
        PArray2 = [0.5 for x in range(self.countElem)]

        # Рассчитываемый вероятностный показатель
        result = 0

        # Режим расчёта значимости указанного элемента
        if (mod == "calc"):

            # Модифицируем вероятности указанного элемента согласно алгоритму расчёта
            PArray1[relevanceId - 1] = 1
            PArray2[relevanceId - 1] = 0

            # Рассчитываем значимость элемента согласно алгоритму расчёта
            result = curPolynom(self.polynomial, PArray1) - curPolynom(self.polynomial, PArray2)

        # Режим расчёта ВБР конфигурации
        elif (mod == "pso"):

            # Если не предоставлены конкретные вероятности элементов, берутся значения по умолчанию
            if (len(pArray) == 0):
                pArray = PArray1

            # Если размерность массива вероятности соответствует расчётной
            if (len(pArray) == len(PArray1)):
                # Рассчитываем ВБР конфигурации
                result = curPolynom(self.polynomial, pArray)
            # Иначе ошибка
            else:
                raise Exception('Неверные вхоные данные для расчёта ВБР')

        # Возвращаем рассчитанный показатель
        return result

# Чистка массива решений от доминируемых решений и повторов
def cleaningSolutions(solutions): # Неотфильтрованный массив

    # Массив отфильтрованных решений
    clearSolutions = []

    # Цикл по массиву решений
    for iS in range(0, len(solutions)):
        # Отсеивание решений с ВБР равным 0
        if (solutions[iS][5] == 0):
            continue

        # Флаг фильтра
        flagS = True

        # Цикл сравнения с остальными решениями на доминируемость
        for jS in range(0, len(solutions)):
            # Пропуск текущего решения
            if (iS == jS):
                continue
            # Если найдено доминирующее решение
            if (solutions[iS][5] <= solutions[jS][5] and solutions[iS][8] > solutions[jS][8] and solutions[iS][10] >= solutions[jS][10]
                or solutions[iS][5] <= solutions[jS][5] and solutions[iS][8] >= solutions[jS][8] and solutions[iS][10] > solutions[jS][10]):
                # Отметка флага и выход из цикла
                flagS = False
                break

        # Если пройдена проверка на доминируемость
        if (flagS):
            # Поиск в отфильтрованных решениях дубликатов
            for jS in range(0, len(clearSolutions)):
                # Если дубликат в отфильтрованных решениях найден
                if (solutions[iS][5] == clearSolutions[jS][5] and solutions[iS][8] == clearSolutions[jS][8] and solutions[iS][10] == clearSolutions[jS][10]):
                    # Отметка флага и выход из цикла
                    flagS = False
                    break

        # Если пройдены обе проверки
        if (flagS):
            # Текущее решение добавляется в отфильтрованные
            clearSolutions.append(solutions[iS])

    return clearSolutions

# Вывод множества решений
def writeSolutions(solutions, algN): #,
                   # Вспомогательные массивы для тестирования алгоритмов
                   # tempArray1=[], tempArray2=[], tempArray3=[], tempArray4=[], tempArray5=[], tempArray6=[], tempArray7=[], tempArray8 = [], tempArray9 = [], tempArray10 = [], maxPC = 0, maxC = 0, maxV = 0):

                   # # Форматирование пути до директории с программой
                   # dir_path = os.path.dirname(os.path.realpath(__file__))
                   # addPath = "/"
                   # if (platform.system() == 'Windows'):
                   #     addPath = "\\"
                   # elif (platform.system() == 'Linux'):
                   #     addPath = "/"
                   # dir_path += addPath

    # Форматирование пути до директории с программой
    dir_path = os.path.dirname(os.path.realpath(__file__))
    addPath = "/"
    if (platform.system() == 'Windows'):
        addPath = "\\"
    elif (platform.system() == 'Linux'):
        addPath = "/"
    dir_path += addPath

    # Запись ответа в .csv файл
    # Инициализация файла
    f = open(dir_path + "result_" + str(algN) + ".csv", "w")

    # Запись заголовка
    f.write(";;ОУИС1;ОУИС2;ОУИС3;ОУИС4;УДМ1;УДМ2;УДМ3;УДМ4;ССН;ОЗД1;ОЗД2;ПОЗ1;ПОЗ2;МА1;МА2;ЭМ1;ЭМ2;ЭМ3;;" + "\n")

    # Печать вспомогательной информации
    # # --------------------------------------------------------------------------------------------------------------
    # # test copy
    #
    # if (algN == 2):
    #
    #     printStrConfigMax = ""
    #     printStrEMax = ""
    #     printStrMMax = ""
    #     for layer in range(0, len(tempArray8)):
    #         for elem in range(0, len(tempArray8[layer])):
    #             printStrConfigMax += str(tempArray8[layer][elem]) + ";"
    #             printStrEMax += str(tempArray9[layer][elem]).replace(".", ",") + ";"
    #             printStrMMax += str(tempArray10[layer][elem]).replace(".", ",") + ";"
    #
    #     f.write("XMax;;;;;;;;;;;;;;;;;;;;;\n")
    #     f.write("P=" + str(round(maxPC, 8)) + ";Построенный полином;" + printStrConfigMax + "\n")
    #     f.write(";Энергопотребление;" + printStrEMax + "C=" + str(round(maxC, 8)).replace(".", ",") + ";" + "\n")
    #     f.write(";Масса;" + printStrMMax + ";V=" + str(round(maxV, 8)).replace(".", ",") + "\n")
    #     f.write(";" + "\n")
    #
    #     for test in range(0, len(tempArray1)):
    #
    #         for example in range(0, len(tempArray1[test])):
    #
    #             printStrConfig = ""
    #             printStrE = ""
    #             printStrM = ""
    #             for layer in range(0, len(tempArray1[test][example])):
    #                 for elem in range(0, len(tempArray1[test][example][layer])):
    #                     printStrConfig += str(tempArray1[test][example][layer][elem]) + ";"
    #                     printStrE += str(tempArray4[test][example][layer][elem]).replace(".", ",") + ";"
    #                     printStrM += str(tempArray6[test][example][layer][elem]).replace(".", ",") + ";"
    #
    #             f.write("k1=" + str(test + 1) + ";Pmax-e1*k=" + str(round(tempArray3[test], 8)) + ";;;;;;;;;;;;;;;;;;;;\n")
    #             f.write("P=" + str(round(tempArray2[test][example], 8)) + ";Построенный полином;" + printStrConfig + "\n")
    #             f.write(";Энергопотребление;" + printStrE + "C=" + str(round(tempArray5[test][example], 8)).replace(".", ",") + ";" + "\n")
    #             f.write(";Масса;" + printStrM + ";V=" + str(round(tempArray7[test][example], 8)).replace(".", ",") + "\n")
    #             f.write(";" + "\n")
    #
    # # --------------------------------------------------------------------------------------------------------------

    # Цикл по решениям
    for solution in solutions:

        # Запись решения в заданном формате
        # В зависимости от алгоритма меняются заголовки
        if (algN == 1):
            f.write("k1=" + str(solution[1]) + ";k2=" + str(solution[2]) + ";;;;;;;;;;;;;;;;;;;Cmin+dC=" + str(solution[3]).replace(".", ",") + ";Vmin+dV=" + str(solution[4]).replace(".", ",") + "\n")
            f.write("P=" + str(round(solution[5], 8)).replace(".", ",") + ", SUM(SUM((a*x))=" + str(solution[11]).replace(".", ",") + ";Построенный полином;" + solution[6] + "\n")
            f.write(";Энергопотребление;" + solution[7] + "C=" + str(round(solution[8], 2)).replace(".", ",") + ";" + "\n")
            f.write(";Масса;" + solution[9] + ";V=" + str(round(solution[10], 2)).replace(".", ",") + "\n")

        elif (algN == 2):
            f.write("k1=" + str(solution[1]) + ";k2=" + str(solution[2]) + ";;;;;;;;;;;;;;;;;;;Pmax-e1*k=" + str(solution[3]).replace(".", ",") + ";Cmin+e2*l=" + str(solution[4]).replace(".", ",") + "\n")
            f.write(";Построенный полином;" + solution[6] + "P=" + str(round(solution[5], 8)).replace(".", ",") + "\n")
            f.write(";Энергопотребление;" + solution[7] + ";C=" + str(round(solution[8], 8)).replace(".", ",") + "\n")
            f.write("V=" + str(round(solution[10], 8)).replace(".", ",") + ";Масса;" + solution[9] + "\n")

        f.write(";" + "\n")

    # Закрытие файла
    f.close()

# Метод форматирования массива ответов в выходной массив
def reformattingResult(curMKA, solutions):

    # Выходной массив
    resultArray = []

    # Цикл по всем решением
    for solutionId in range(0, len(solutions)):

        # Расширение массива словарём текущего решение
        resultArray.append({})

        # Добавление ВБР решения
        resultArray[solutionId]['p'] = round(solutions[solutionId][5], 8)
        # Добавление энергоёмкости решения
        resultArray[solutionId]['energy_sum'] = round(solutions[solutionId][8], 2)
        # Добавление массы решения
        resultArray[solutionId]['weight_sum'] = round(solutions[solutionId][10], 2)

        # Конвертация списка выбранных решений в список id элементов
        # Список выбранных решений
        solArray = list(map(int, solutions[solutionId][6][:-1].replace('-', '-1').split(';')))
        resultArray[solutionId]['config'] = []
        # Цикл по элементам
        for elemId in range(0, len(solArray)):
            # Если элемент не подобран, проставляем значение -1 и идём в следующую итерацию
            if (solArray[elemId] == -1):
                resultArray[solutionId]['config'].append(-1)
                continue
            # Флаг останова при проходе по циклу
            breakFlag = False
            # Цикл по конфигурации
            for layer in range(0, len(curMKA.config)):
                # Цикл по элментам слоя
                for elem in curMKA.config[layer]:
                    # Если найдена соответствующая позиция в конфигурации
                    if (elem == elemId + 1):
                        # Сохраняем соответствующий id
                        resultArray[solutionId]['config'].append(curMKA.stockIdArray[layer][solArray[elemId] - 1])
                        # Отмечаем флаг и выходим из цикла
                        breakFlag = True
                        break
                # Если элемент найден, выходим
                if (breakFlag):
                    break

    # Возврат ответа
    return resultArray

# ---------------------------------------------------------------------------------------------------------------------
# Алгоритм 1
# ---------------------------------------------------------------------------------------------------------------------

# Расчёт показателей ограничений для решения задач линейного программирования (ЛП)
def calcIndicators(curMKA, # Текущий МКА
                   k): # Размерность по количеству задач ЛП

    # Значения показателей по умолчанию
    eMax = 0
    eMin = 0
    mMax = 0
    mMin = 0

    # Цикл по слоям конфигурации
    for layer in range(0, len(curMKA.config)):

        # Расчёт индекса элемента с максимальной ВБР
        iMax = curMKA.pArray[layer].index(max(curMKA.pArray[layer]))
        # Раскомментировать, если нужно считать от минимального значения, а не от 0
        # Расчёт индекса элемента с минимальной ВБР
        # iMin = curMKA.pArray[layer].index(min(curMKA.pArray[layer]))

        # Расчёт массы и энергоёмкости элемента с максимальной ВБР
        eMax += curMKA.eArray[layer][iMax] * len(curMKA.config[layer])
        mMax += curMKA.mArray[layer][iMax] * len(curMKA.config[layer])

        # Раскомментировать, если нужно считать от минимального значения, а не от 0
        # Расчёт массы и энергоёмкости элемента с минимальной ВБР
        # eMin += curMKA.eArray[layer][iMin] * len(curMKA.config[layer])
        # mMin += curMKA.mArray[layer][iMin] * len(curMKA.config[layer])

    # Вычисление уступок по энергоёмкости и массе
    de = math.fabs(eMax - eMin) / (k - 1)
    dm = math.fabs(mMax - mMin) / (k - 1)

    # Возврат уступок по энергоёмкости и массе и минимальных заданных значений
    return de, dm, eMin, mMin

# Итерация цикла - решение задачи ЛП
def calcIter(curMKA, # Текущий МКА
             de, # Ограничение энергоёмкости
             dm): # Ограничение массы

    # Входные данные для расчёта значимости элементов
    # id текущего элемента
    relevanceId = 1
    # массив значимостей
    relevanceArray = []

    # Цикл по всем элементам
    while (relevanceId <= curMKA.countElem):

        # Цикл по слоям конфигурации
        for i in range(0, len(curMKA.config)):

            # Добавляем слой в массив значимостей
            relevanceArray.append([])

            # Цикл по элементам слоя конфигурации
            for j in range(0, len(curMKA.config[i])):

                # Вычисление и запись текущего элемента конфигурации
                relevanceArray[i].append(curMKA.probabilitiesCalc(relevanceId))

                relevanceId += 1

    # Составление целевой и граничных функций задачи ЛП

    xArray = [] # Массив переменных ЛП
    relevanceId = 1 # id текущего элемента
    targetFunction = 0 # Значение целевой функции по умолчанию
    eFunction = 0 # Значение функции ограничения энергоёмкости по умолчанию
    mFunction = 0 # Значение функции ограничения массы по умолчанию
    borderFunctions = [] # Массив прочих граничных функций ЛП

    # Цикл по элементам конфигурации
    while (relevanceId <= curMKA.countElem):

        # Добавление слоя в массив переменных ЛП (варианты реализации текущего элемента)
        xArray.append([])
        # Запись значения по умолчанию в граничную функцию ЛП текущей итерации
        borderFunctions.append(0)

        # Инициализация индексов целевой функции
        targetI = -1
        targetJ = -1

        # Поиск координат элемента текущей итерации
        # Цикл по слоям конфигурации
        for i in range(0, len(curMKA.config)):

            # Цикл по элементам текущего слоя конфигурации
            for j in range(0, len(curMKA.config[i])):

                # Если id текущего элемента конфигурации равно id конкретного элемента конкретного слоя
                if (relevanceId == curMKA.config[i][j]):

                    # Запоминаем координаты найдено элемента и выходим из цикла
                    targetI = i
                    targetJ = j
                    break

            # Если координаты были найдены - выходим
            if (targetI >= 0):
                break

        # Цикл формирования функций
        # Цикл по вариантам реализации текущего элемента
        for i in range(0, len(curMKA.elementsArray[targetI])):

            # Инициализация текущего варианта реализации
            xArray[relevanceId - 1].append(pulp.LpVariable("x" + str(relevanceId) + "_" + str(i + 1), lowBound=0, cat='Integer'))

            # Расширение целевой функции текущим вариантом реализации согласно заданному алгоритму
            targetFunction += (curMKA.pArray[targetI][i] * relevanceArray[targetI][targetJ]) * xArray[relevanceId - 1][i]
            # Расширение функции ограничения энергоёмкости согласно заданному алгоритму
            eFunction += curMKA.eArray[targetI][i] * xArray[relevanceId - 1][i]
            # Расширение функции ограничения массы согласно заданному алгоритму
            mFunction += curMKA.mArray[targetI][i] * xArray[relevanceId - 1][i]
            # Расширение граничной функции текущей итерации согласно заданному алгоритму
            borderFunctions[relevanceId - 1] += xArray[relevanceId - 1][i]

        relevanceId += 1

    # Инициализация задачи ЛП
    problem = pulp.LpProblem('0', pulp.LpMaximize)
    # Ввод целевой функции
    problem += targetFunction, "Функция цели"
    # Ввод всех граничных ограничений
    k = 1
    for i in range(0, len(borderFunctions)):
        # Из всех вариантов реализации может быть выбран один или не выбрано ничего
        problem += borderFunctions[i] <= 1, str(k)
        k += 1
    # Ввод ограничения энергоёмкости (не больше заданной)
    problem += eFunction <= de, str(k)
    k += 1
    # Ввод ограничения массы (не больше заданной
    problem += mFunction <= dm, str(k)
    k += 1
    # Ввод ограничений по элементам
    for array in xArray:
        for elem in array:
            # Каждый элемент может быть выбран или не выбран (запрет на дробные значения)
            problem += elem <= 1, str(k)
            k += 1
            problem += elem >= 0, str(k)
            k += 1
    # Решение описанной задачи
    problem.solve()

    # Получение списка решений
    tempResultLp = problem.variables()

    # Переформатирование результата в массив
    # (пришлось ввести костыль из-за сбитой сортировки в списке решений самой библиотеки)

    # Инициализация словаря решений
    tempResultDict = {}
    # Заполнение словаря решений
    for elem in tempResultLp:
        tempResultDict[elem.name] = elem.varValue

    # Инициализация итогового массива решений
    resultArray = []
    # Заполнение массива согласно заданной последовательности элементов
    for i in range(0, len(xArray)):
        resultArray.append([])
        for j in range(0, len(xArray[i])):
            resultArray[i].append(tempResultDict[xArray[i][j].name])

    # Формирование массива решений заданного формата
    # Массив решений
    selectElements = []
    # Id текущего решения
    resultId = 0
    # Цикл по слоям конфигурации
    for i in range(0, len(curMKA.config)):
        # Расширение массива решений новым слоем
        selectElements.append([])
        # Цикл по элементам текущего слоя конфигурации
        for j in range(0, len(curMKA.config[i])):
            # Флаг, по которому определяется, был ли добавлен элемент
            loopFlag = False
            # Цикл по элментам слоя решений
            for k in range(0, len(resultArray[resultId])):
                # Если найден ненулевой элемент слоя решений
                if (resultArray[resultId][k] > 0):
                    # Записывается идентификатор этого элемента
                    selectElements[i].append(curMKA.elementsArray[i][k])
                    # Отмечаем все флаги и выходим из цикла
                    resultId += 1
                    loopFlag = True
                    break

            # Если в слое решений элемент не выбран (в решение отсутствует конкретный модуль)
            # переходим к следующему элементу
            if (not loopFlag):
                resultId += 1

    # Возврат массива решений и статуса решения задачи ЛП
    return selectElements, problem.status

# Внешний цикл алгоритма
def calcMainFirst(curMKA, # Текущий МКА
             kIn): # Ограничение по количеству решаемых задач

    # Вычисление размерности
    k = int(math.floor(math.sqrt(kIn)))

    # Расчёт уступок и минимальных значений энергоёмкости и массы
    de, dm, eMin, mMin = calcIndicators(curMKA, k)

    # Количество нерешённых задач
    failCalc = 0
    # Общее количество задач (проверочная переменная)
    allCalcId = 0

    # Массив решений
    solutions = []

    # Решение всех задач ЛП (ответ формируется в виде .csv файла)
    # Цикл по уступкам энергоёмкости
    for k1 in range(0, k):

        # Цикл по уступкам массы
        for k2 in range(0, k):

            # Вычисление ограничений текущей задачи ЛП
            curDE = eMin + de * k1
            curDM = mMin + dm * k2

            # Решение текущей задачи ЛП
            selectElements, status = calcIter(curMKA, curDE, curDM)

            # Если задача была решена
            if (status >= 0):

                # Ответ по конфигурации
                resultElements = ""
                # Ответ по энергоёмкости элементов
                resultEArray = ""
                # Ответ по массе элементов
                resultMArray = ""
                # Массив ответа по ВБР элементов
                resultPArray = []
                # Общая энергоёмкость
                resultE = 0
                # Общая масса
                resultM = 0
                # ВБР конфигурации (классическое определение)
                resultP = 1

                # Цикл по слоям конфигурации
                for i in range(0, len(curMKA.config)):
                    # Цикл по элементам слоя конфигурации
                    for j in range(0, len(curMKA.config[i])):
                        # Если для текущего элемента есть решение (для слота выбран вариант реализации)
                        if (j < len(selectElements[i])):

                            # Формирование строки ответа по конфигурации
                            resultElements += str(selectElements[i][j]) + ";"

                            # Учёт энергоёмкости выбранного элемента
                            resultE += curMKA.eArray[i][selectElements[i][j] - 1]
                            # Формирование строки ответа по энергоёмкости элемента
                            resultEArray += str(curMKA.eArray[i][selectElements[i][j] - 1]) + ";"

                            # Учёт массы выбранного элемента
                            resultM += curMKA.mArray[i][selectElements[i][j] - 1]
                            # Формирование строки ответа по массе элемента
                            resultMArray += str(curMKA.mArray[i][selectElements[i][j] - 1]) + ";"

                            # Учёт ВБР выбранного элемента
                            resultP *= curMKA.pArray[i][selectElements[i][j] - 1]
                            # Формирование массива ответа по ВБР элементов
                            resultPArray.append(curMKA.pArray[i][selectElements[i][j] - 1])
                        # Если для текущего элемента отсутствует решение (слот варианта реализации оставлен пустым)
                        else:
                            # Ответы заполняются нулевыми значениями
                            resultElements += "-;"
                            resultEArray += "-;"
                            resultMArray += "-;"
                            resultPArray.append(0)

                # Форматирование ответов
                resultElements = resultElements.replace(".", ",")
                resultEArray = resultEArray.replace(".", ",")
                resultMArray = resultMArray.replace(".", ",")

                # ВБР конфигурации (по полиному)
                resultP2 = curMKA.probabilitiesCalc(0, "pso", resultPArray)

                # Форматирование параметров конфигурации
                P = round(resultP2, 8)
                # Раскомментировать, если нужен классический расчёт ВБР
                # P = round(resultP, 8)
                E = round(resultE, 2)
                M = round(resultM, 2)

                # Расчёт вспомогательного параметра значимости конфигурации
                # Инициализация переменной значимости конфигурации
                targetFSum = 0
                # Расчёт значимости конфигурации по заданному алгоритму
                for testI in range(0, curMKA.countElem):
                    targetFSum += resultPArray[testI] * curMKA.probabilitiesCalc(testI + 1)
                # Форматирование ответа
                targetFSum = round(targetFSum, 4)

                # Запись решения текущей задачи ЛП
                solutions.append([True, # Succes flag
                                  k1, # k1
                                  k2, # k2
                                  round(curDE, 2), # Cmin+dC
                                  round(curDM, 2), # Vmin+dV
                                  P, # P
                                  resultElements, # Построенный полином
                                  resultEArray, # Энергопотребление
                                  E, # C
                                  resultMArray, # Масса
                                  M, # V
                                  targetFSum]) # ΣΣa*x
            # Если задача не была решена
            else:
                # Пометка провала
                failCalc += 1
                # Запись ответа с ошибочным решением
                solutions.append([False,  # Succes flag
                                  k1, # k1
                                  k2, # k2
                                  round(curDE, 2), # Cmin+dC
                                  round(curDM, 2), # Vmin+dV
                                  -1,  # P
                                  [],  # Построенный полином
                                  [],  # Энергопотребление
                                  0,  # C
                                  [],  # Масса
                                  0, # V
                                  0]) # ΣΣa*x

            # Отметка прохождения итерации решения
            allCalcId += 1

    # Фильтрация решений
    clearSolutions = cleaningSolutions(solutions)

    # Переформатирование результата
    resultArray = reformattingResult(curMKA, clearSolutions)

    # # Запись ответа в файл
    # writeSolutions(clearSolutions, 1)

    # # Вывод вспомогательной информации по алгоритму
    # print("Всего первым алгоритмом решено " + str(allCalcId) + " задач")
    # print("Не найдено решений для " + str(failCalc) + " задач")
    # print("")

    return resultArray

# ---------------------------------------------------------------------------------------------------------------------
# Алгоритм 2
# ---------------------------------------------------------------------------------------------------------------------

# Дополнительный алгоритм для случайного направленного поиска
def randomDirectionalSearch(curMKA, # Текущий МКА
                            xMaxPArray, # Исходная конфигурация
                            pMaxArray, # ВБР элементов исходной конфигурации
                            dP, # Уступка
                            M = 10): # Число статистических испытаний

    # Цикл по заданному количеству испытаний
    s = 0
    # Множество найденных решений
    d1 = []
    # Вероятности найденных решений
    d1PArray = []

    while s < M:

        # Обнуление стартовой конфигурации
        xCur = []
        for layer in range(0, len(xMaxPArray)):
            xCur.append([])
            for elem in range(0, len(xMaxPArray[layer])):
                xCur[layer].append(xMaxPArray[layer][elem])
        dPCur = curMKA.probabilitiesCalc(0, 'pso', pMaxArray)
        pArrayCur = []
        for elem in range(0, len(pMaxArray)):
            pArrayCur.append(pMaxArray[elem])
        # Решение, добавляемое в множество решений
        xAdd = []
        pAdd = 0

        # Цикл по попадению в уступку (задан алгоритмом)
        while dPCur > dP:

            # Решение, добавляемое в множество решений
            # (запоминаем последнюю удачную конфигурацию, поскольку выход из цикла будет при неудовлетворительном xCur)
            xAdd = []
            for layer in range(0, len(xCur)):
                xAdd.append([])
                for elem in range(0, len(xCur[layer])):
                    xAdd[layer].append(xCur[layer][elem])
            pAdd = curMKA.probabilitiesCalc(0, 'pso', pArrayCur)

            # Массив разниц вероятностей
            dpArray = []
            # Идентификатор текущего элемента
            elemId = 0
            # Массив идентификаторов заменяемых типов
            replaceableTypeArray = []
            # Массив ВБР заменяемых типов
            replaceablePArray = []

            # Нахождение изменения надёжности для каждого элемента
            # Цикл по слоям конфигурации
            for i in range(0, len(xCur)):

                # Расширение массивов конфигурации
                dpArray.append([])
                replaceableTypeArray.append([])

                # Цикл по элементам слоя конфигурации
                for j in range(0, len(xCur[i])):

                    # Искомое минимальное изменение надёжности
                    dpMin = -1
                    dpMinId = 0

                    # Цикл по вариантам реализации элементов
                    for elem in range(0, len(curMKA.pArray[i])):

                        # Если совпадают выбранные элементы идём на следующую итерацию
                        if (elem == xCur[i][j]):
                            continue

                        # Если вероятность заменяемого элемента меньше вероятности заменяющего, то пропускаем
                        if (curMKA.pArray[i][xCur[i][j]] < curMKA.pArray[i][elem]):
                            continue

                        # Вычисление изменения надёжности по заданной формуле
                        curdp = curMKA.probabilitiesCalc(elemId) * (curMKA.pArray[i][xCur[i][j]] - curMKA.pArray[i][elem])

                        # Запись наименьшего изменения надёжности
                        if (dpMin == -1 or curdp < dpMin):
                            dpMin = curdp
                            dpMinId = elem

                    # Расширение массива
                    # Если новая конфигурация была найдена
                    if (dpMin > -1):
                        dpArray[i].append(1 - dpMin)
                        replaceableTypeArray[i].append(dpMinId)
                        replaceablePArray.append(curMKA.pArray[i][dpMinId])
                    # Иначе
                    else:
                        dpArray[i].append(0)
                        replaceableTypeArray[i].append(-1)
                        replaceablePArray.append(-1)

                    elemId += 1

            # Сумма отказов
            L = 0
            for dpLayer in dpArray:
                L += sum(dpLayer)

            # Построение массива интервалов
            bArray = []
            # Цикл по слоям
            for dpLayerI in range(0, len(dpArray)):
                # Расширение массива интервалов
                bArray.append([])
                # Цикл по элементам слоя
                for dp in dpArray[dpLayerI]:
                    bArray[dpLayerI].append(dp / L)

            # Задание рандомного значения
            rand = random.random()

            # Определение диапазона, в который попало случайное значение
            # Искомые координаты замены
            replaceableItemI = 0
            replaceableItemJ = 0
            replaceableP = 0
            # Текущее пороговое значени
            w = 0
            # Флаг выхода из цикла
            breakFlag = False
            # Цикл по слоям
            for bI in range(0, len(bArray)):
                # Цикл по элементам слоя
                for bJ in range(0, len(bArray[bI])):
                    # Приращенее порогового значения текущим интервалом
                    w += bArray[bI][bJ]
                    # Проверка на попадание в текущий интервал
                    if (rand <= w):
                        # Сохранение координат и выход из цикла
                        replaceableItemI = bI
                        replaceableItemJ = bJ
                        breakFlag= True
                        break
                    # Иначе приращаем идентификатор массива вероятностей
                    replaceableP += 1
                if (breakFlag):
                    break

            # Замена определённого элемента на определённый тип
            xCur[replaceableItemI][replaceableItemJ] = replaceableTypeArray[replaceableItemI][replaceableItemJ]
            pArrayCur[replaceableP] = replaceablePArray[replaceableP]

            # Вычисление надёжности полученной конфигурации
            dPCur = curMKA.probabilitiesCalc(0, 'pso', pArrayCur)

        # Добавляем полученное решение в множество решений и переходим на следующий шаг
        d1.append(xAdd)
        d1PArray.append(pAdd)
        s += 1

    # Чистка полученного множества от повторов
    d1Clear = []
    d1PArrayClear = []
    for solutionId in range(0, len(d1)):
        if (d1[solutionId] not in d1Clear and len(d1[solutionId]) > 0):
            d1Clear.append(d1[solutionId])
            d1PArrayClear.append(d1PArray[solutionId])

    # Возвращаем полученное множество решений
    return d1Clear, d1PArrayClear

# Метод форматирования параметров решения
def formattingSolution(solutionIn, # Форматируемое решение
                       PIn, # ВБР решения
                       k1, # k1
                       k2, # k2
                       dP, # Pmax+e1*k
                       dC): # Cmin+e2*l

    # Ответ по конфигурации
    resultElements = ""
    # Ответ по энергоёмкости элементов
    resultEArray = ""
    # Ответ по массе элементов
    resultMArray = ""
    # Общая энергоёмкость
    resultE = 0
    # Общая масса
    resultM = 0

    # Строим ответы
    # Цикл по слоям
    for solutionI in range(0, len(solutionIn)):
        # Цикл по элементам слоя
        for solutionElem in solutionIn[solutionI]:
            # Считаем переменные
            resultElements += str(solutionElem) + ";"
            resultEArray += str(curMKA.eArray[solutionI][solutionElem]) + ";"
            resultE += curMKA.eArray[solutionI][solutionElem]
            resultMArray += str(curMKA.mArray[solutionI][solutionElem]) + ";"
            resultM += curMKA.mArray[solutionI][solutionElem]

    # Форматирование ответов
    resultElements = resultElements.replace(".", ",")
    resultEArray = resultEArray.replace(".", ",")
    resultMArray = resultMArray.replace(".", ",")

    # Форматирование параметров конфигурации
    P = round(PIn, 8)
    E = round(resultE, 2)
    M = round(resultM, 2)

    # Формирование текущего решения
    return [True, # Succes flag
              k1, # k1
              k2, # k2
              round(dP, 8), # Pmax-e1*k
              round(dC, 2), # Cmin+e2*l
              P, # P
              resultElements, # Построенный полином
              resultEArray, # Энергопотребление
              E, # C
              resultMArray, # Масса
              M, # V
              0]

# Внешний цикл алгоритма
def calcMainSecond(curMKA, # Текущий МКА
                   kIn, # Ограничение по количеству решаемых задач
                   MIn = 10): # Число статистических испытаний

    # Вычисление размерности
    k = int(math.floor(math.sqrt(kIn)))

    # Значения показателей по умолчанию
    # Массивы конфигураций
    xMaxPArray = []
    xMinPArray = []
    maxPArray = []
    minPArray = []

    # Нахождение конфигураций с максимальной и минимальной ВБР
    # Цикл по слоям конфигурации
    for layer in range(0, len(curMKA.config)):

        # Добавляем слой в массивы конфигураций
        xMaxPArray.append([])
        xMinPArray.append([])

        # Цикл по элементам слоя конфигурации
        for j in range(0, len(curMKA.config[layer])):

            # Находим максимальные и минимальные вероятности для текущего типа элементов
            maxP = max(curMKA.pArray[layer])
            minP = min(curMKA.pArray[layer])

            # Добавляем найденные элементы в массивы конфигурации
            xMaxPArray[layer].append(curMKA.pArray[layer].index(maxP))
            xMinPArray[layer].append(curMKA.pArray[layer].index(minP))
            maxPArray.append(maxP)
            minPArray.append(minP)

    # Считаем ВБР максимальной и минимальной конфигураций
    maxPC = curMKA.probabilitiesCalc(0, 'pso', maxPArray)
    minPC = curMKA.probabilitiesCalc(0, 'pso', minPArray)

    # Уступка по вероятности
    # e1 = (maxPC - minPC) / (k - 1) # Старая версия
    e1 = maxPC - minPC # Новая версия

    # Массив решений
    solutions = []

    # Генерация вспомогательной информации
    # # --------------------------------------------------------------------------------------------------------------
    # # test copy
    #
    # eMax = 0
    # mMax = 0
    # eMaxArray = []
    # mMaxArray = []
    # xMaxArray = []
    #
    # for layer in range(0, len(xMaxPArray)):
    #     eMaxArray.append([])
    #     mMaxArray.append([])
    #     xMaxArray.append([])
    #     for elem in range(0, len(xMaxPArray[layer])):
    #         eMaxArray[layer].append(curMKA.eArray[layer][xMaxPArray[layer][elem]])
    #         eMax += curMKA.eArray[layer][xMaxPArray[layer][elem]]
    #         mMaxArray[layer].append(curMKA.mArray[layer][xMaxPArray[layer][elem]])
    #         mMax += curMKA.mArray[layer][xMaxPArray[layer][elem]]
    #         xMaxArray[layer].append(xMaxPArray[layer][elem])
    #
    # d1ArrayCopy = []
    # d1PArrayCopy = []
    # d1BorderArrayCopy = []
    # d1EArrayCopy = []
    # d1MArrayCopy = []
    # d1ESumArrayCopy = []
    # d1MSumArrayCopy = []
    #
    # # --------------------------------------------------------------------------------------------------------------

    # Цикл по надёжности
    for k1 in range(1, k):

        # Получение первого множества решений
        d1, d1PArray = randomDirectionalSearch(curMKA,
                                               xMaxPArray,
                                               maxPArray,
                                               # maxPC - e1 * k1) # Старая версия
                                               maxPC - e1 / pow(2, k - k1), # Новая версия
                                               M=MIn)
                                               # M=(160 - k * 10))

        # Генерация вспомогательной информации
        # # --------------------------------------------------------------------------------------------------------------
        # # test copy
        #
        # d1ArrayCopy.append([])
        # d1EArrayCopy.append([])
        # d1MArrayCopy.append([])
        # d1ESumArrayCopy.append([])
        # d1MSumArrayCopy.append([])
        # d1PArrayCopy.append([])
        #
        # for example in range(0, len(d1)):
        #     d1ArrayCopy[k1 - 1].append([])
        #     d1EArrayCopy[k1 - 1].append([])
        #     d1MArrayCopy[k1 - 1].append([])
        #     eSum = 0
        #     mSum = 0
        #     tempPArray = []
        #
        #     for layer in range(0, len(d1[example])):
        #         d1ArrayCopy[k1 - 1][example].append([])
        #         d1EArrayCopy[k1 - 1][example].append([])
        #         d1MArrayCopy[k1 - 1][example].append([])
        #
        #         for elem in range(0, len(d1[example][layer])):
        #             d1ArrayCopy[k1 - 1][example][layer].append(d1[example][layer][elem])
        #             d1EArrayCopy[k1 - 1][example][layer].append(curMKA.eArray[layer][d1[example][layer][elem]])
        #             eSum += curMKA.eArray[layer][d1[example][layer][elem]]
        #             d1MArrayCopy[k1 - 1][example][layer].append(curMKA.mArray[layer][d1[example][layer][elem]])
        #             mSum += curMKA.mArray[layer][d1[example][layer][elem]]
        #             tempPArray.append(curMKA.pArray[layer][d1[example][layer][elem]])
        #
        #     d1ESumArrayCopy[k1 - 1].append(eSum)
        #     d1MSumArrayCopy[k1 - 1].append(mSum)
        #
        #     d1PArrayCopy[k1 - 1].append(curMKA.probabilitiesCalc(0, "pso", tempPArray))
        #
        # # for elem in range(0, len(d1PArray)):
        # #     d1PArrayCopy[k1 - 1].append(d1PArray[elem])
        #
        # # d1BorderArrayCopy.append(maxPC - e1 * k1) # Старая версия
        # d1BorderArrayCopy.append(maxPC - e1 / pow(2, k - k1)) # Новая версия
        #
        # # --------------------------------------------------------------------------------------------------------------

        # Если в полученном множестве решения отсутствуют, переходим к следующей итерации
        if (len(d1) == 0):
            continue

        # Если в полученном множестве одно решение, то запоминаем его и переходим к следующей итерации
        if (len(d1) == 1):
            solutions.append(formattingSolution(d1[0],
                                                d1PArray[0],
                                                k1,
                                                0,
                                                # maxPC - e1 * k1, # Старая версия
                                                maxPC - e1 / pow(2, k - k1), # Новая версия
                                                0))
            continue

        # Значения показателей по умолчанию
        # Массивы конфигураций
        maxC = -1
        minC = -1

        # Массив энергоёмкостей решений множества
        d1CArray = []

        # Нахождение максимальной и минимальной энергоёмкостоей
        # Цикл по решениям
        for curSolution in d1:

            # Энергоёмкость текущего решения
            curC = 0

            # Цикл по слоям
            for layer in range(0, len(curSolution)):
                # Цикл по элементам слоя
                for elem in curSolution[layer]:
                    # Суммирование энергоёмкостей элементов
                    curC += curMKA.eArray[layer][elem]

            # Проверка на максимальное значение и присвоение
            if (maxC < curC or maxC == -1):
                maxC = curC
            # Проверка на минимальное значение и присвоение
            if (minC > curC or minC == -1):
                minC = curC
            # Запоминам энергоёмкость текущего решения
            d1CArray.append(curC)

        # Вычисляем уступку по энергоёмкости
        e2 = (maxC - minC) / (k - 1)

        # Цикл по энергоёмкости
        for k2 in range(1, k - 1):

            # Второе множество решений
            d2 = []
            d2PArray = []

            # Генерация второго множества решений по заданному алгоритму
            for solutionId in range(0, len(d1)):

                # Выбор элементов второго множества по заданному условию
                if (d1CArray[solutionId] <= minC + k2 * e2):
                    d2.append(d1[solutionId])
                    d2PArray.append(d1PArray[solutionId])

            # Если в полученном множестве решения отсутствуют, переходим к следующей итерации
            if (len(d2) == 0):
                continue
            # Если в полученном множестве одно решение, то запоминаем его и переходим к следующей итерации
            if (len(d2) == 1):
                solutions.append(formattingSolution(d2[0],
                                                    d2PArray[0],
                                                    k1,
                                                    k2,
                                                    # maxPC - e1 * k1, # Старая версия
                                                    maxPC - e1 / pow(2, k - k1), # Новая версия
                                                    minC + k2 * e2))
                continue

            # Нахождение решения с минимальной массой
            minV = -1
            minVId = 0

            # Цикл по решениям
            for curSolutionId in range(0, len(d2)):

                # Масса текущего решения
                curV = 0

                # Цикл по слоям
                for layer in range(0, len(d2[curSolutionId])):
                    # Цикл по элементам слоя
                    for elem in d2[curSolutionId][layer]:
                        # Суммирование массы элементов
                        curV += curMKA.mArray[layer][elem]

                # Проверка на минимальное значение и присвоение
                if (minV > curV or minV == -1):
                    minV = curV
                    minVId = curSolutionId

            # Добавляем найденное решение в множество эффективных решений
            solutions.append(formattingSolution(d2[minVId],
                                                d2PArray[minVId],
                                                k1,
                                                k2,
                                                # maxPC - e1 * k1, # Старая версия
                                                maxPC - e1 / pow(2, k - k1), # Новая версия
                                                minC + k2 * e2))

    # Фильтрация решений
    clearSolutions = cleaningSolutions(solutions)

    # Переформатирование результата
    resultArray = reformattingResult(curMKA, clearSolutions)

    # # Запись ответа в файл
    # writeSolutions(clearSolutions, 2,
    #
    #                 # Печать вспомогательной информации
    #                 # --------------------------------------------------------------------------------------------------------------
    #                 # test copy
    #                tempArray1=d1ArrayCopy, tempArray2=d1PArrayCopy, tempArray3=d1BorderArrayCopy, tempArray4=d1EArrayCopy, tempArray5=d1ESumArrayCopy, tempArray6=d1MArrayCopy, tempArray7=d1MSumArrayCopy,
    #                tempArray8=xMaxArray, tempArray9=eMaxArray, tempArray10=mMaxArray, maxPC=maxPC, maxC=eMax, maxV=mMax)
    #                 # --------------------------------------------------------------------------------------------------------------

    # # Вывод вспомогательной информации по алгоритму
    # print("Всего вторым алгоритмом решено " + str(len(solutions)) + " задач")
    # print("")

    return resultArray

# ---------------------------------------------------------------------------------------------------------------------
# Главная функция
# ---------------------------------------------------------------------------------------------------------------------

# Main function
if __name__=="__main__":

    # Инициализация текущиего МКА
    curMKA = apiMKA(averageTime= os.getenv("averTime", averTime),
                    polynomialName=os.getenv("polName", polName))

    # # Вывод вспомогательной информации
    # print("Входной полином: " + str(curMKA.config))
    # print("Доступные элементы: " + str(curMKA.elementsArray))
    # print("Энергоэффективность доступных элементов: " + str(curMKA.eArray))
    # print("Стоимость доступных элементов: " + str(curMKA.mArray))
    # print(" ")

    # Режим запуска алгоритмов
    curAlg = os.getenv("iAlg", iAlg)

    # Максимальное количество решений
    k = os.getenv("kSet", kSet)

    # Массивы ответов
    resultFirst = []
    resultSecond = []

    begin = datetime.now()

    if (curAlg in (1, 3)):

        resultFirst = calcMainFirst(curMKA, k)

    if (curAlg in (2, 3)):

        resultSecond = calcMainSecond(curMKA, k, MIn=os.getenv("MSet", MSet))

    print("Время работы: " + str(datetime.now() - begin))
